/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;


public class RegularMovie extends Movie {
    private static final double BASE_AMOUNT = 2;
    private static final double DAYS_EXCEEDED = 2;
    private static final double AMOUNT_FOR_EXCESS_DAYS = 1.5;

    public RegularMovie(String title) {
        super(title);
    }

    @Override
    public double getPrice(int daysRented) {
        double amount = BASE_AMOUNT;
        if (daysRented > DAYS_EXCEEDED)
            amount += (daysRented - DAYS_EXCEEDED) * AMOUNT_FOR_EXCESS_DAYS;
        return amount;
    }
}
