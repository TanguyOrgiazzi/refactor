/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

public class NewReleaseMovie extends Movie {
    private static final double BASE_AMOUNT = 3;

    public NewReleaseMovie(String title) {
        super(title);
    }

    @Override
    public double getPrice(int daysRented) {
        return daysRented * BASE_AMOUNT;
    }
}
