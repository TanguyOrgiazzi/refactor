/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;



public class ChildrenMovie extends Movie {
    private static final double BASE_AMOUNT = 1.5;
    private static final double DAYS_EXCEEDED = 3;

    public ChildrenMovie(String title) {
        super(title);
    }

    @Override
    public double getPrice(int daysRented) {
        double amount = BASE_AMOUNT;
        if (daysRented > DAYS_EXCEEDED)
            amount += (daysRented - DAYS_EXCEEDED) * BASE_AMOUNT;
        return amount;
    }
}
