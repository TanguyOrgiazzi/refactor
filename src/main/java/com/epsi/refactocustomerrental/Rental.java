/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

public class Rental {
    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public String getMovieTitle() {
        return movie.getTitle();
    }

    public int getRenterPoints() {
        return (isNewRelease(movie) && isRentedForMoreThanOneDay()) ? 2 : 1;
    }

    private boolean isNewRelease(Movie movie) {
        return (movie instanceof NewReleaseMovie);
    }

    private boolean isRentedForMoreThanOneDay() {
        return daysRented > 1;
    }

    public double getAmount() {
        return movie.getPrice(daysRented);
    }
}