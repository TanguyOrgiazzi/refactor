/*

 * To change this template, choose Tools | Templates

 * and open the template in the editor.

 */

package com.epsi.refactocustomerrental;

import java.util.*;


public class Customer {
    private String name;
    private List<Rental> rentals;
    private IStatement statement;

    public Customer(String name, List<Rental> rentals, IStatement statement) {
        this.name = name;
        this.rentals = rentals;
        this.statement = statement;
    }

    public String statement() {
        return this.statement.claculStatement(this.name, this.rentals);
    }
}