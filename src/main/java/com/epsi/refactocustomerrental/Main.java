/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.slf4j.simple.SimpleLogger;


public class Main {

    public static void main(String[] a) {

        final SimpleLogger logger = (SimpleLogger) LoggerFactory.getLogger(Main.class);

        Movie newReleaseMovie = new NewReleaseMovie("NRM_Return IV");

        Movie regularMovie = new RegularMovie("RM_Back II");

        List<Rental> rentals = new ArrayList<>();
        
        Rental r1 = new Rental(newReleaseMovie, 5);

        rentals.add(r1);

        rentals.add(new Rental(regularMovie, 5));
        
        Customer customer = new Customer("Tanguy", rentals, new HtmlStatement());
        Customer customer2 = new Customer("Test", rentals, new BasicStatement());

        logger.info(customer.statement());
        logger.info(customer2.statement());

    }

}
 