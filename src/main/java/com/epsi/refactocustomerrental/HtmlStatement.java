/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import java.util.List;


public class HtmlStatement implements IStatement {

    @Override
    public String claculStatement(String name, List<Rental> rentals) {
        StringBuilder statement = new StringBuilder();
        statement.append("\n<H1>Rental record for ").append(name).append(" :</H1>\n");
        getIndividualRentalSummary(statement, rentals);
        statement.append("<H2>Amount owed is ").append(getTotalAmount(rentals)).append(" !\n");
        statement.append("You earned ").append(getFrequentRenterPoints(rentals)).append(" frequent renter points.</H2>\n");

        return statement.toString();
    }
    
    private void getIndividualRentalSummary(StringBuilder statement, List<Rental> rentals) {
        rentals.forEach((rental) -> {
            statement.append("<p>").append("\t").append(rental.getMovieTitle()).append("\t").append(rental.getAmount()).append("</p>").append("\n");
        });
    }

    private double getTotalAmount(List<Rental> rentals) {
        return rentals.stream().mapToDouble(Rental::getAmount).sum();
    }

    private int getFrequentRenterPoints(List<Rental> rentals) {
        return rentals.stream().mapToInt(Rental::getRenterPoints).sum();
    }
}
