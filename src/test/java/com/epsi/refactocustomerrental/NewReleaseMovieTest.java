/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import org.junit.Test;
import static org.junit.Assert.*;

public class NewReleaseMovieTest {

    /**
     * Test of getPrice method, of class NewReleaseMovie.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        int daysRented = 6;
        NewReleaseMovie instance = new NewReleaseMovie("SAO");
        double expResult = 24.0;
        double result = instance.getPrice(daysRented);
        assertEquals(expResult, result, 24.0);
    }
    
}
