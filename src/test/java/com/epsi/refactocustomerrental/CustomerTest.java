/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antoine
 */
public class CustomerTest {

    /**
     * Test of statement method, of class Customer.
     */
    @Test
    public void testStatement() {
        System.out.println("basic statement");
        List<Rental> rentals = new ArrayList<>();
        rentals.add(new Rental(new ChildrenMovie("SAO"), 7));
        Customer customer = new Customer("Tanguy", rentals, new BasicStatement());
        String expResult = "\nRental record for Tanguy :\n\tSAO\t7.5\nAmount owed is 7.5 !\nYou earned 1 frequent renter points.\n";
        String result = customer.statement();
        assertEquals(expResult, result);
        System.out.println("html statement");
        Customer customer2 = new Customer("Test", rentals, new HtmlStatement());
        expResult = "\n<H1>Rental record for Test :</H1>\n<p>\tSAO\t7.5</p>\n<H2>Amount owed is 7.5 !\nYou earned 1 frequent renter points.</H2>\n";
        result = customer2.statement();
        assertEquals(expResult, result);
    }
    
}
