/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antoine
 */
public class RentalTest {

    /**
     * Test of getMovieTitle method, of class Rental.
     */
    @Test
    public void testGetMovieTitle() {
        System.out.println("getMovieTitle");
        Rental rental = new Rental(new ChildrenMovie("SAO"), 2);
        String expResult = "SAO";
        String result = rental.getMovieTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenterPoints method, of class Rental.
     */
    @Test
    public void testGetRenterPoints() {
        System.out.println("getRenterPoints");
        //For Regular et Children movies :
        Rental rentalForNormalMovie = new Rental(new ChildrenMovie("SAO"), 2);
        int expResultForNormalMovie = 1;
        int resultexpResultForNormalMovie = rentalForNormalMovie.getRenterPoints();
        assertEquals(expResultForNormalMovie, resultexpResultForNormalMovie);
        
        //For NewReleaseMovies :
        //For dayRented > 1 :
        Rental rentalForNewReleaseMovie = new Rental(new NewReleaseMovie("SAO"), 4);
        int expResultForNewReleaseMovie = 2;
        int resultForNewReleaseMovie = rentalForNewReleaseMovie.getRenterPoints();
        assertEquals(expResultForNewReleaseMovie, resultForNewReleaseMovie);
        //For dayRented < 1 :
        rentalForNewReleaseMovie = new Rental(new NewReleaseMovie("SAO"), 1);
        expResultForNewReleaseMovie = 1;
        resultForNewReleaseMovie = rentalForNewReleaseMovie.getRenterPoints();
        assertEquals(expResultForNewReleaseMovie, resultForNewReleaseMovie);
    }

    /**
     * Test of getAmount method, of class Rental.
     */
    @Test
    public void testGetAmount() {
        System.out.println("getAmount");
        Rental rental = new Rental(new ChildrenMovie("sao"), 2);
        double expResult = 2.0;
        double result = rental.getAmount();
        assertEquals(expResult, result, 2.0);
    }
    
}
