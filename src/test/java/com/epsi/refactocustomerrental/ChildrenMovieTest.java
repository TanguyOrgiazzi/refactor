/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import org.junit.Test;
import static org.junit.Assert.*;


public class ChildrenMovieTest {

    /**
     * Test of getPrice method, of class ChildrenMovie.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        ChildrenMovie childrenMovie = new ChildrenMovie("SAO");
        //exeded days
        int daysRented = 7;
        double expResult = 10.5;
        double result = childrenMovie.getPrice(daysRented);
        assertEquals(expResult, result, 10.5);
        
        //exeded days
        daysRented = 2;
        expResult = 1.5;
        result = childrenMovie.getPrice(daysRented);
        assertEquals(expResult, result, 10.5);
    }
    
}
