/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.refactocustomerrental;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antoine
 */
public class RegularMovieTest {
    
    /**
     * Test of getPrice method, of class RegularMovie.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        int daysRented = 5;
        RegularMovie regularMovie = new RegularMovie("SAO");
        double expResult = 10.0;
        double result = regularMovie.getPrice(daysRented);
        assertEquals(expResult, result, 10.0);
        
        daysRented = 1;
        expResult = 2;
        result = regularMovie.getPrice(daysRented);
        assertEquals(expResult, result, 2);
    }
    
}
